<?php

namespace App\Controller;

use DateTimeImmutable;
use App\Entity\DataFile;
use App\Form\ExcelFileType;
use App\Service\FileUploader;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DataFileController extends AbstractController
{

    private $appKernel;
    
    public function __construct(KernelInterface $appKernel)
    {
        $this->appKernel = $appKernel;
    }

    /**
     * @Route("/", name="app_data_file")
     */
    public function index(EntityManagerInterface $entityManager,FileUploader $file,Request $request): Response
    {
        $form =  $this->createForm(ExcelFileType::class); //create formtype to send to twig
        $form->handleRequest($request);

        $check = false;
        $error =  null;

        if ($form->isSubmitted()  && $form->isValid())    //handle form submission
        {
            $file->setTargetDirectory($this->getParameter('excel'));  //Set the directory of the file 

            $filePathName = $form->get('fichier')->getData();     //get data uploaded
            $filePathName = $file->upload($filePathName);         // upload the file 

            $fileFolder = $this->appKernel->getProjectDir() . '/public/uploads/excel/';

            $spreadsheet = IOFactory::load($fileFolder . $filePathName); // Here we are able to read from the excel file 
            // dd($spreadsheet->getActiveSheet()->getHighestDataRow());
            if($spreadsheet->getActiveSheet()->getHighestDataRow() != 64) //check if the uploaded file is the right one
            {
                $error = 'Veuillez sélectionner le bon fichier (TestTechnique-Import (1) (1).xlsx)';
                return $this->render('data_file/index.html.twig', [
                    'form' => $form->createView(),
                    'check' => $check,
                    'error' => $error ? $error : null
                ]);     // send variables to the response to the twig
              
            }
            $row = $spreadsheet->getActiveSheet()->removeRow(64); // I added this to be able to remove the last file line  
            $row = $spreadsheet->getActiveSheet()->removeRow(1); // I added this to be able to remove the first file line

            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true); // here, the read data is turned into an array
            foreach ($sheetData as $Row) 
            { 
                $compteAffaire = $Row['A']; // get the data rows by rows
                $compteEvenement = $Row['B'];
                $compteDernierEvenement= $Row['C'];    
                $numeroFiche = $Row['D'];  
                $libelleCivilite = $Row['E']; 
                $proprietaireVehicule = $Row['F'];
                $nom= $Row['G'];    
                $prenom = $Row['H'];   
                $numeroNomVoie = $Row['I']; 
                $complementAdresse1 = $Row['J'];
                $codePostal= $Row['K'];    
                $ville = $Row['L'];   
                $telephoneDomicile = $Row['M']; 
                $telephonePortable = $Row['N'];
                $telephoneJob= $Row['O'];    
                $email = $Row['P'];   
                $dateMiseCirculation = $Row['Q']; 
                $dateAchat = $Row['R'];
                $dateDernierEvenement= $Row['S'];    
                $libelleMarque = $Row['T'];   
                $libelleModele = $Row['U']; 
                $version = $Row['V'];
                $vin = $Row['W'];
                $immatriculation= $Row['X'];    
                $typeProspect = $Row['Y'];  
                $kilometrage = $Row['Z']; 
                $libelleEnergie = $Row['AA'];
                $vendeurVn= $Row['AB'];    
                $vendeurVo = $Row['AC'];   
                $commentaireFacturation = $Row['AD']; 
                $typeVnVo = $Row['AE'];
                $numeroDossierVnVo= $Row['AF'];    
                $intermediaireVenteVn = $Row['AG'];   
                $dateEvenement = $Row['AH'];
                $origineEvenement = $Row['AI'];
                
                $data = new DataFile();  // instantiate new entity DataFile

                $data->setCompteAffaire($compteAffaire);       //set DataFile entity attributes to file values    
                $data->setCompteEvenement($compteEvenement); 
                $data->setCompteDernierEvenement($compteDernierEvenement);  
                $data->setNumeroFiche((int)$numeroFiche);
                $data->setLibelleCivilite($libelleCivilite); 
                $data->setProprietaireVehicule($proprietaireVehicule); 
                $data->setNom($nom);
                $data->setPrenom($prenom); 
                $data->setNumeroNomVoie($numeroNomVoie); 
                $data->setComplementAdresse1($complementAdresse1); 
                $data->setCodePostal((int)$codePostal);
                $data->setVille($ville);
                $data->setTelephoneDomicile($telephoneDomicile); 
                $data->setTelephonePortable($telephonePortable);
                $data->setTelephoneJob($telephoneJob);
                $data->setEmail($email);
                $data->setDateMiseCirculation( $dateMiseCirculation ? new DateTimeImmutable($dateMiseCirculation) : null); 
                $data->setDateAchat( new DateTimeImmutable($dateAchat));
                $data->setDateDernierEvenement($dateDernierEvenement ? new DateTimeImmutable($dateDernierEvenement) : null); 
                $data->setLibelleMarque($libelleMarque);
                $data->setLibelleModele($libelleModele);
                $data->setVersion($version);
                $data->setVin($vin);
                $data->setImmatriculation($immatriculation); 
                $data->setTypeProspect($typeProspect);
                $data->setKilometrage((int)$kilometrage);
                $data->setLibelleEnergie($libelleEnergie);
                $data->setVendeurVn($vendeurVn); 
                $data->setVendeurVo($vendeurVo); 
                $data->setCommentaireFacturation($commentaireFacturation); 
                $data->setTypeVnVo($typeVnVo);
                $data->setNumeroDossierVnVo($numeroDossierVnVo);   
                $data->setIntermediaireVenteVn($intermediaireVenteVn);
                $data->setDateEvenement($dateEvenement ? new DateTimeImmutable($dateEvenement) : null);
                $data->setOrigineEvenement($origineEvenement);

                $entityManager->persist($data); 
                $entityManager->flush(); 
                // here Doctrine checks all the fields of all fetched data and make a transaction to the database.
            }     
            $check = true;    
        }
        return $this->render('data_file/index.html.twig', [
            'form' => $form->createView(),
            'check' => $check,
            'error' => $error ? $error : null
        ]);
    }
}
