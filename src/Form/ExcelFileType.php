<?php 

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ExcelFileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        // builder for formtype(ExcelFileType)
        $builder
        ->add('fichier',FileType::class,[
            'label' => 'Fichier (.xlsx)',
            'mapped' => false,
            'required' => true,
            'constraints' => [
                new File([
                    'maxSize' => '1024k',       // define max size for file
                    'maxSizeMessage' => 'Fichier trop volumineux',
                    'mimeTypes' => [
                        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                        'application/vnd.ms-excel',
                        'application/octet-stream',
                        // 'application/pdf'
                ],
                    'mimeTypesMessage' => 'Veuillez télécharger un fichier valide (.xls,.xlsx))', // error message
                    'maxSizeMessage' => 'Fichier trop volumineux: 1024 kb au maximum'
                ])
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
