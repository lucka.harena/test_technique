<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230301221058 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE data_file (id INT AUTO_INCREMENT NOT NULL, compte_affaire VARCHAR(50) NOT NULL, compte_evenement VARCHAR(50) NOT NULL, compte_dernier_evenement VARCHAR(50) NOT NULL, numero_fiche INT NOT NULL, libelle_civilite VARCHAR(10) DEFAULT NULL, proprietaire_vehicule VARCHAR(50) DEFAULT NULL, nom VARCHAR(50) NOT NULL, prenom VARCHAR(50) DEFAULT NULL, numero_nom_voie VARCHAR(100) DEFAULT NULL, complement_adresse1 VARCHAR(50) DEFAULT NULL, code_postal INT NOT NULL, ville VARCHAR(50) NOT NULL, telephone_domicile VARCHAR(25) DEFAULT NULL, telephone_portable VARCHAR(25) DEFAULT NULL, telephone_job VARCHAR(50) DEFAULT NULL, email VARCHAR(50) DEFAULT NULL, date_mise_circulation DATE DEFAULT NULL, date_achat DATE DEFAULT NULL, date_dernier_evenement DATE NOT NULL, libelle_marque VARCHAR(25) NOT NULL, libelle_modele VARCHAR(25) DEFAULT NULL, version VARCHAR(75) DEFAULT NULL, vin VARCHAR(50) NOT NULL, immatriculation VARCHAR(25) DEFAULT NULL, type_prospect VARCHAR(15) NOT NULL, kilometrage INT DEFAULT NULL, libelle_energie VARCHAR(15) DEFAULT NULL, vendeur_vn VARCHAR(50) DEFAULT NULL, vendeur_vo VARCHAR(50) DEFAULT NULL, commentaire_facturation VARCHAR(75) DEFAULT NULL, type_vn_vo VARCHAR(5) DEFAULT NULL, numero_dossier_vn_vo VARCHAR(25) DEFAULT NULL, intermediaire_vente_vn VARCHAR(50) DEFAULT NULL, date_evenement DATE NOT NULL, origine_evenement VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE data_file');
    }
}
